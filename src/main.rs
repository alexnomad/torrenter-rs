mod search;

use crate::search::SearchResult;
use std::{io, env};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    let mut buf = get_query()?;

    let page = search::get_page(&buf).await?;
    let search_results = search::get_results(&page).await?;

    if search_results.len() <= 0 {
        println!("No results");
        return Ok(());
    }
    pretty_print_search(&search_results);

    println!("Select one to download: (1..{})", search_results.len());
    buf = String::new();
    io::stdin().read_line(&mut buf)?;
    let mut index: usize = buf.trim().parse()?;
    index -= 1;

    let magnet = search::get_magnet(&search_results[index].link).await?;
    println!("Magnet for the {}th one: {}", index + 1, &magnet);

    Ok(())
}


fn get_query() -> anyhow::Result<String> {
    let mut search = String::new();

    let args: Vec<String> = env::args().skip(1).collect();
    if args.is_empty() {
        println!("Search torrent: ");
        io::stdin().read_line(&mut search)?;
        return Ok(search);
    }

    for arg in &args {
        search += &(" ".to_owned() + arg);
    }
    search = search.trim().to_owned();
    Ok(search)
}

fn pretty_print_search(search_results: &Vec<SearchResult>) {
    println!("\n\n----------------------------------------------------------------\n");
    let mut i = 1;
    for result in search_results {
        if i % 2 == 0 {
            println!("{}", console::style(
                format!("{i}: {}\nseeds: {}, peers: {}, {}", result.name, result.seeds, result.peers, result.size)
            ).color256(12));
        } else {
            println!("{}", console::style(
                format!("{i}: {}\nseeds: {}, peers: {}, {}", result.name, result.seeds, result.peers, result.size)
            ).color256(11));
        }
        i += 1;
    }
    println!("\n----------------------------------------------------------------");
}
