use anyhow::Context;

#[derive(Debug)]
pub struct SearchResult {
    pub name: String,
    pub seeds: u16,
    pub peers: u16,
    pub size: String,
    pub link: String,
}

pub async fn get_page(q: &String) -> anyhow::Result<scraper::Html> {
    let body = reqwest::get(format!("https://1337x.proxyninja.org/srch?search={q}"))
        .await?
        .text()
        .await?;

    Ok(scraper::Html::parse_document(&body))
}

pub async fn get_results(page: &scraper::Html) -> anyhow::Result<Vec<SearchResult>> {
    let seeds_selector = scraper::Selector::parse(r#"tr > td.seeds"#      ).unwrap();
    let peers_selector = scraper::Selector::parse(r#"tr > td.leeches"#    ).unwrap();
    let sizes_selector = scraper::Selector::parse(r#"tr > td.size"#       ).unwrap();
    let names_selector = scraper::Selector::parse(r#"td > a:nth-child(2)"#).unwrap();

    let seeds: Vec<scraper::ElementRef> = page.select(&seeds_selector).collect();
    let peers: Vec<scraper::ElementRef> = page.select(&peers_selector).collect();
    let sizes: Vec<scraper::ElementRef> = page.select(&sizes_selector).collect();
    let names: Vec<scraper::ElementRef> = page.select(&names_selector).collect();

    let mut search_results: Vec<SearchResult> = Vec::new();

    // would work nice so long all Vectors are the same size (which should be always the case)
    for i in 0..seeds.len() {
        let seed: u16    = seeds[i].text().next().context("parsing error")?.to_string().parse()?;
        let peer: u16    = peers[i].text().next().context("parsing error")?.to_string().parse()?;
        let size: String = sizes[i].text().next().context("parsing error")?.to_string();
        let name: String = names[i].text().next().context("parsing error")?.to_string();
        let link: String = "https://1337x.proxyninja.org".to_string() + names[i].value().attr("href").context("no href")?;

        search_results.push(SearchResult {
            name,
            seeds: seed,
            peers: peer,
            size,
            link,
        });
    }

    Ok(search_results)
}

/// Gets magnet link from a 1337x link
pub async fn get_magnet(link: &String) -> anyhow::Result<String> {
    let body = reqwest::get(link)
        .await?
        .text()
        .await?;
    let page = scraper::Html::parse_document(&body);

    let magnet_selector = scraper::Selector::parse(r#"div > div > div > ul > li:nth-child(1) > a:nth-child(1)"#).unwrap();
    let magnet_el = page.select(&magnet_selector).next().context("no magnet element")?;
    let magnet_link = magnet_el.value().attr("href").context("no href for magnet")?.to_string();
    Ok(magnet_link)
}
