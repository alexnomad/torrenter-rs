# 1337x CLI
## Usage
`x1337-cli <search query>`

I also recomend you try [mpv-webtorrent-hook](https://github.com/noctuid/mpv-webtorrent-hook) as it provides convinient way to play magnet links directly in mpv

`mpv <magnet link>`


## Building

### Pre-requirements
- cargo
- git

1. `git clone https://gitlab.com/alexnomad/x1337-cli`

2. `cd x1337-cli`

##### Auto
3. `./install.sh`

##### Manual

3. `cargo build --release`

4. `sudo cp target/release/x1337-cli /usr/bin/`

OR 

4. `cp target/release/x1337-cli ~/.local/bin/`

